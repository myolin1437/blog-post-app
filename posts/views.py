from django.shortcuts import render, redirect

from posts.models import Post
from posts.forms import CreatePost

# Create your views here.
def list_posts(request):
    posts = Post.objects.all()
    context = {
        "posts": posts,
    }
    return render(request, "posts/main.html", context)

def create_post(request):
    form = CreatePost()
    if request.method == "POST":
        form = CreatePost(request.POST)
        if form.is_valid():
            form.save()
            return redirect("posts_list")
    context = {
        "form": form,
    }
    return render(request, "posts/create.html", context)