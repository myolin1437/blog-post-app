from django.forms import ModelForm

from posts.models import Post

class CreatePost(ModelForm):
    class Meta:
        model = Post
        fields = "__all__"